﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFailState : MonoBehaviour
{
    public GameObject death;
    public GameObject gameUI;

    bool a;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameObject.Find("GameController").GetComponent<SelectToken1>().moveRotatedToken == true || GameObject.Find("GameController").GetComponent<SelectToken1>().moveToken == true)
        {
            GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            GetComponent<BoxCollider>().enabled = true;
        }
    }

    private void OnTriggerEnter(Collider collision)
    {        
        if (collision.gameObject.tag == "Token" || collision.gameObject.tag == "RotatedToken")
        {
            Destroy(GameObject.Find("Select(Clone)"));
            Destroy(GameObject.Find("SelectRotated(Clone)"));
            gameUI.SetActive(false);
            death.SetActive(true);
            GameObject.Find("GameController").GetComponent<SelectToken1>().fall = true;
        }
    }


}
