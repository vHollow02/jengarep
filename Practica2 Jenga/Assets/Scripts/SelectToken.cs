﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectToken : MonoBehaviour
{
    public bool moveToken;
    public bool moveRotatedToken;
    RaycastHit objectHit = new RaycastHit();
    int randomNumber;
    int randomDirection;
    public int nextPosition;
    public int gameMode;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {     
        if (moveToken == false & moveRotatedToken == false) { 
            if (Input.GetMouseButtonDown(0))      
            {                    
                bool checkForHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out objectHit);
           
                if (checkForHit) {
                    Debug.Log("Hit " + objectHit.transform.gameObject.name);

                    if (objectHit.transform.gameObject.tag == "Token")
                    {
                        moveToken = true;
                        randomNumber = Random.Range(10, 20);
                        randomDirection = Random.Range(1, 3);
                        objectHit.transform.gameObject.tag = "Untagged";
                    }
                    if (objectHit.transform.gameObject.tag == "RotatedToken")
                    {
                        randomDirection = Random.Range(1, 3);
                        moveRotatedToken = true;
                        randomNumber = Random.Range(10, 20);
                        objectHit.transform.gameObject.tag = "Untagged";
                    }
                }
            }
        }


        if (moveToken == true)
        {
            switch (randomDirection) {
                case 1:
                    if (objectHit.transform.position.z > -randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y, objectHit.transform.position.z - 0.1f);
                        }
                    }        
                    else
                    {                       
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                switch (nextPosition)
                                {
                                    case 1:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(2.52f, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;                                        
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 2;
                                        moveToken = false;
                                        break;
                                    case 2:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 3;
                                        moveToken = false;
                                        break;
                                    case 3:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(-2.52f, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 4;
                                        moveToken = false;
                                        break;
                                    case 4:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 5;
                                        moveToken = false;
                                        break;
                                    case 5:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 6;
                                        moveToken = false;
                                        break;
                                    case 6:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, -2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 1;
                                        moveToken = false;                                        
                                        break;
                                }
                            }
                        }
                        else
                        {
                            moveToken = false; 
                        }
                    }
                    break;

                case 2:
                    if (objectHit.transform.position.z < randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y, objectHit.transform.position.z + 0.1f);
                        }
                    }
                    else
                    {
                        
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                switch (nextPosition)
                                {
                                    case 1:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(2.52f, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 2;
                                        moveToken = false;
                                        break;
                                    case 2:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 3;
                                        moveToken = false;
                                        break;
                                    case 3:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(-2.52f, 31, 0);
                                        objectHit.transform.localRotation = new Quaternion(0, 0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 4;
                                        moveToken = false;
                                        break;
                                    case 4:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 5;
                                        moveToken = false;
                                        break;
                                    case 5:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 6;
                                        moveToken = false;
                                        break;
                                    case 6:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31,-2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 1;
                                        moveToken = false;
                                        break;
                                }
                            }                            
                        }
                        else
                        {
                            moveToken = false; ;
                        }
                    }
                    break;
            }
        }


        if (moveRotatedToken == true)
        {
            switch (randomDirection)
            {
                case 1:
                    if (objectHit.transform.position.x > -randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x - 0.1f, objectHit.transform.position.y, objectHit.transform.position.z );
                        }
                    }
                    else
                    {
                       
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                switch (nextPosition)
                                {
                                    case 1:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(2.52f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.rotation.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 2;
                                        moveRotatedToken = false;
                                        break;
                                    case 2:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 3;
                                        moveRotatedToken = false;
                                        break;
                                    case 3:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(-2.52f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.eulerAngles.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 4;
                                        moveRotatedToken = false;
                                        break;
                                    case 4:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 5;
                                        moveRotatedToken = false;
                                        break;
                                    case 5:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 6;
                                        moveRotatedToken = false;
                                        break;
                                    case 6:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, -2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 1;
                                        moveRotatedToken = false;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            moveRotatedToken = false;
                        }
                    }
                    break;

                case 2:
                    if (objectHit.transform.position.x < randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x + 0.1f, objectHit.transform.position.y, objectHit.transform.position.z);
                        }
                    }
                    else
                    {
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                switch (nextPosition) {
                                    case 1:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(2.52f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.rotation.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 2;
                                        moveRotatedToken = false;
                                        break;
                                    case 2:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.rotation.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 3;
                                        moveRotatedToken = false;
                                        break;
                                    case 3:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(-2.52f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, objectHit.transform.rotation.y+90, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "Token";
                                        nextPosition = 4;
                                        moveRotatedToken = false;
                                        break;
                                    case 4:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 5;
                                        moveRotatedToken = false;
                                        break;
                                    case 5:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, 0);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 6;
                                        moveRotatedToken = false;
                                        break;
                                    case 6:
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                                        objectHit.transform.position = new Vector3(0f, 31, -2.52f);
                                        objectHit.transform.localRotation = Quaternion.Euler(0, 0, 0);
                                        objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                                        objectHit.transform.gameObject.tag = "RotatedToken";
                                        nextPosition = 1;
                                        moveRotatedToken = false;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            moveRotatedToken = false;
                        }
                    }
                    break;
            }
        }

    }
}
