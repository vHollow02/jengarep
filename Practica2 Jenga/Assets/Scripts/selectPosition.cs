﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selectPosition : MonoBehaviour
{
    public Material selected;
    public Material notSelected;

    // Start is called before the first frame update
    void Start()
    {

        if (this.gameObject.name.Contains("token1"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token1 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }
        if (this.gameObject.name.Contains("token2"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token2 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }
        if (this.gameObject.name.Contains("token3"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token3 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }
        if (this.gameObject.name.Contains("token4"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token4 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }
        if (this.gameObject.name.Contains("token5"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token5 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }
        if (this.gameObject.name.Contains("token6"))
        {
            if (GameObject.Find("GameController").GetComponent<SelectToken1>().token6 == false)
            {
                this.gameObject.GetComponent<MeshRenderer>().material = selected;
            }
            else
            {
                this.gameObject.GetComponent<MeshRenderer>().material = notSelected;
            }
        }




    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
